Kalman filter for mpu6050 (Simulation)

The aim of this C++ project is to implement a Kalman filter which can provide fast and accurate solution for angle measurement with MPU6050 Inertia Measurement Unit (IMU). The result is compared with a simple complementary filter. 

The MPU-6050 sensor contains a MEMS accelerometer and a MEMS gyro in a single chip. It is very accurate, as it contains 16-bits analog to digital conversion hardware for each channel. Therefore, it captures the x, y, and z channel at the same time. The sensor uses the I2C-bus to interface with other devices.

Kalman filter is a discrete, optimal state estimator with the assumption of the system has process and measurement noises which are independent of each other, white, and with normal, zero-mean probability distribution.

The project is an example. Every major phenomenon is simulated in the code: movement of the sensor, sensor noises, sensor bias etc. The Kalman filter was implemented in two different ways: 1) with traditions C functions (single value operations); 2) with an included class (matrixClass) which is a C++ approach, due to the class, matrix operations can be used directly -> shorter computational code and more general solution (7 lines instead of 27). This project does not require any input: the main.cpp can be compiled and run easily. The author used Code::Blocks, gnu gcc, C++ 11.

References:
Kalman, R. E., A New Approach to Linear Filtering and Prediction Problems. Transaction of the ASME-Journal of Basic Engineering, pp. 35-45. (1960)

Welch, G., Bishop, G., An Introduction to the Kalman Filter. University of North Carolina. (1997)

Kelly, Don, Kalman Filter for 6DOF IMU Implementation (6/6) https://www.youtube.com/watch?v=N6AEYUSAE6E (2017)

Furkanicus, How to Create a Matrix Class Using C++. https://medium.com/@furkanicus/how-to-create-a-matrix-class-using-c-3641f37809c7 (2015)
