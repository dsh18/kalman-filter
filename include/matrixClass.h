#ifndef MATRIXCLASS_H
#define MATRIXCLASS_H

#include <iostream>
#include <vector>

class matrixClass
{
    private:

        unsigned m_rowSize;
        unsigned m_colSize;
        std::vector<std::vector<double>> m_matrix;

    public:
        //Constructors, destructor
        matrixClass(unsigned, unsigned, double);
        matrixClass(const matrixClass &);
        ~matrixClass();
        //Matrix Operations
        matrixClass operator+(matrixClass );
        matrixClass operator-(matrixClass );
        matrixClass operator*(matrixClass );
        matrixClass transpose();
        //matrixClass inv();
        //Scalar Operations
        matrixClass operator+(double);
        matrixClass operator-(double);
        matrixClass operator*(double);
        matrixClass operator/(double);
        //Functions
        void print() const;
        unsigned getRows() const;
        unsigned getCols() const;
        double& operator()(const unsigned &, const unsigned &);
};

#endif // MATRIXCLASS_H
