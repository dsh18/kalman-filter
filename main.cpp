#include <iostream>
#include <cmath>
#include "matrixClass.h"

float complementaryFilter(float gainK1, float gainK2, float prevAngle, float gyroMeask1, float accelMeask1, float deltaT);

int main()
{
    //Simulation variables
    float rollVelTrue = 0.0;                //The true value of the Roll angle velocity (for simulation)
    float rollTruek = 0.0;                  //The true k value of the Roll angle (for simulation)
    float rollTruek1 = 0.0;                 //The true k+1 value of the Roll angle (for simulation)
    float gyroDriftTrue = 1.0;              //The drift of the (Roll) gyroscope
    float gyroCalBiasTrue = 0.01;           //The bias of the (Roll) gyroscope
    float gyroSigmaNoise = 0.002;           //The noise of the (Roll) gyroscope
    float accelSigmaNoise = sqrt(0.03);     //The noise of the accelerometer
    //Simulation times
    float deltaT = 0.004;                   //Time step (ms), the simulation runs on 250Hz
    float time = 0.0;                       //Current simulation time
    int endpoint = 2500;                    //Length of the simulation (ms)
    //MPU6050 measurement variables
    float accelMeask1 = 0.0;                //The simulated k+1 accelerometer measurement
    float gyroMeask1 = 0.0;                 //The simulated k+1 gyroscope measurement
    float gyroReadk1 = 0.0;                 //The simulated gyroscope measurement with drift and noise
    //Complementary filter variables
    float angleCompk1 = 0.0;                //Complementary filter k+1 angle
    float angleCompk = 0.0;                 //Complementary filter k angle
    //Kalman filter variables (0th elements of the vectors/arrays are always 0 in order to be consistent with the MATLAB code)
    float pk[] = {0, 0.5, 0, 0, 0.1};       //Covariance matrix k, how well we are doing state estimate
    float pk1Minus[] = {0, 0.5, 0, 0, 0.1};
    float pk1[] = {0, 0.5, 0, 0, 0.1};      //Covariance matrix k
    float xk[] = {0, 0, 0};                 //State vector k: estimated angle, gyro drift
    float xk1Minus[] = {0, 0, 0};
    float xk1[] = {0, 0, 0};                //State vector k+1: estimated angle, gyro drift
    float K[] = {0, 0, 0};                  //Kalman gains
    float phi[] = {0, 1, deltaT, 0, 1};     //State transition matrix
    float psi[] = {0, deltaT, 0};           //Control transition matrix
    float R = 0.03;                         //Measurement noise
    float Q[] = {0, 0.002*0.002, 0, 0, 0};  //Process noise
    float H[] = {0, 1, 0};                  //observation matrix
    float S;
    float uk;                               //gyroscope measurement
    float zk;                               //accelerometer measurement
    float nu;

    //Kalman filter variables for matrix operations (same as previously)
    matrixClass Mxk1Minus = {2, 1, 0};
    matrixClass Mxk  = {2, 1, 0};
    matrixClass Mxk1  = {2, 1, 0};
    matrixClass Mpk1Minus = {2, 1, 0};
    matrixClass Mpk = {2, 2, 0};
    Mpk(0,0) = 0.5;
    Mpk(1,1) = 0.1;
    matrixClass Mpk1  = {2, 2, 0};
    matrixClass MK  = {2, 1, 0};
    matrixClass Mphi = {2, 2, 0};
    Mphi(0,0) = 1.0;
    Mphi(0,1) = deltaT;
    Mphi(1,1) = 1.0;
    matrixClass Mpsi = {2, 1, 0};
    Mpsi(0,0) = deltaT;
    matrixClass MQ = {2, 2, 0};
    MQ(0,0) = 0.002*0.002;
    matrixClass MH = {1, 2, 0};
    MH(0,0) = 1.0;
    matrixClass MI = {2, 2, 0};
    MI(0,0) = 1.0;
    MI(1,1) = 1.0;

    for (int i = 1; i<endpoint; i++)
    {
        //Constructing the true movement of the simulated sensor (ideal angle)
        rollVelTrue = 0.0;
        if ((i>=500) && (i<750)) rollVelTrue = 30.0;
        else if ((i>=1250) && (i<1500)) rollVelTrue = -45.0;
        else if ((i>=1750) && (i<2000)) rollVelTrue = 0.0;

        //Computing the sensor values for the simulation (with noises, bias and drift in order to be realistic)
        rollTruek1 = rollTruek + rollVelTrue * deltaT;
        accelMeask1 = rollTruek1 + ((float) rand() / (RAND_MAX))*accelSigmaNoise;
        gyroReadk1 = rollVelTrue - gyroDriftTrue + ((float) rand() / (RAND_MAX))*gyroSigmaNoise + gyroCalBiasTrue;
        gyroMeask1 = gyroReadk1 - gyroCalBiasTrue;

        //Complementary filter computation
        angleCompk1 = complementaryFilter(0.8, 0.2, angleCompk, gyroMeask1, accelMeask1, deltaT);

        //Kalman Filter computation with single values
        uk = gyroMeask1;                    //gyroscope measurement
        zk = accelMeask1;                   //accelerometer measurement
        //Matrix equation: xk1Minus = phi*xk + psi*uk;
        xk1Minus[1] = phi[1]*xk[1] + phi[2]*xk[2] + psi[1]*uk;
        xk1Minus[2] = phi[3]*xk[1] + phi[4]*xk[2] + psi[2]*uk;
        //Matrix equation: pk1Minus = phi*pk*phi' + Q;
        pk1Minus[1] = (phi[1]*pk[1] + phi[2]*pk[3])*phi[1] + (phi[1]*pk[2] + phi[2]*pk[4])*phi[2] + Q[1];
        pk1Minus[2] = (phi[1]*pk[1] + phi[2]*pk[3])*phi[3] + (phi[1]*pk[2] + phi[2]*pk[4])*phi[4] + Q[2];
        pk1Minus[3] = (phi[3]*pk[1] + phi[4]*pk[3])*phi[1] + (phi[3]*pk[2] + phi[4]*pk[4])*phi[2] + Q[3];
        pk1Minus[4] = (phi[3]*pk[1] + phi[4]*pk[3])*phi[3] + (phi[3]*pk[2] + phi[4]*pk[4])*phi[4] + Q[4];
        //Matrix equation: S = H*pk1Minus*H'+R;
        S = (H[1]*pk1Minus[1] + H[2]*pk1Minus[3])*H[1] + (H[1]*pk1Minus[2] + H[2]*pk1Minus[4])*H[1] + R;
        //Matrix equation: K = pk1Minus*H'*inv(S);
        K[1] = (pk1Minus[1]*H[1] + pk1Minus[2]*H[2])/S;
        K[2] = (pk1Minus[3]*H[1] + pk1Minus[4]*H[2])/S;
        //Matrix equation: xk1 = xk1Minus + K * (zk - H*xk1Minus);
        nu = zk - (H[1]*xk1Minus[1] + H[2]*xk1Minus[2]);
        xk1[1] = xk1Minus[1] + K[1]*nu;
        xk1[2] = xk1Minus[2] + K[2]*nu;
        //Matrix equation: pk1 = (eye(2,2)-K*H)*pk1Minus;
        pk1[1] = (1 - K[1]*H[1])*pk1Minus[1] + (0 - K[1]*H[2])*pk1Minus[3];
        pk1[2] = (1 - K[1]*H[1])*pk1Minus[2] + (0 - K[1]*H[2])*pk1Minus[4];
        pk1[3] = (1 - K[2]*H[1])*pk1Minus[1] + (0 - K[2]*H[2])*pk1Minus[3];
        pk1[4] = (1 - K[2]*H[1])*pk1Minus[2] + (0 - K[2]*H[2])*pk1Minus[4];

        //Kalman filter with matrix operations
        Mxk1Minus = (Mphi*Mxk) + (Mpsi*uk);
        Mpk1Minus = Mphi*Mpk*Mphi.transpose() + MQ;
        S = (MH*Mpk1Minus*MH.transpose()+R)(0,0);
        MK = Mpk1Minus*MH.transpose()/S;
        Mxk1 =  Mxk1Minus + (MK*(zk - (MH*Mxk1Minus)(0,0)));
        Mpk1 = (MI-(MK*MH))*Mpk1Minus;

        //Updating the variables for the next iteration
        rollTruek = rollTruek1;             //For true angle
        angleCompk = angleCompk1;           //For Complementary filter
        xk[1] = xk1[1];                     //For Kalman filter...
        xk[2] = xk1[2];
        pk[1] = pk1[1];
        pk[2] = pk1[2];
        pk[3] = pk1[3];
        pk[4] = pk1[4];
        Mxk = Mxk1;                         //For Kalman filter (matrix)
        Mpk = Mpk1;                         //For Kalman filter (matrix)
        time = time + deltaT;               //For simulation time

        //Printing the true angle and the Kalman filter with matrix operations
        std::cout << rollTruek << " " <<  Mxk(0,0) << std::endl;
    }
    return 0;
}

float complementaryFilter(float gainK1, float gainK2, float prevAngle, float gyroMeask1, float accelMeask1, float deltaT)
{
    //Complementary filter computation: gainK1 - gain of the gyroscope measurement; gainK2 - gain of the accelerometer measurement
    float anglek1 = gainK1*(prevAngle + gyroMeask1*deltaT) + gainK2*accelMeask1;
    return anglek1;
}

