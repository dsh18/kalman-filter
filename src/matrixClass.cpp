#include "matrixClass.h"

//Constructor
matrixClass::matrixClass(unsigned rowSize, unsigned colSize, double initial){
    m_rowSize = rowSize;
    m_colSize = colSize;
    m_matrix.resize(rowSize);
    for (unsigned i = 0; i < m_matrix.size(); i++)
    {
        m_matrix[i].resize(colSize, initial);
    }
}
//Copy Constructor
matrixClass::matrixClass(const matrixClass &B)
{
    this->m_colSize = B.getCols();
    this->m_rowSize = B.getRows();
    this->m_matrix = B.m_matrix;

}
//Destructor
matrixClass::~matrixClass()
{

}

//Matrix Addition
matrixClass matrixClass::operator+(matrixClass B){
    matrixClass sum(m_rowSize, m_colSize, 0.0);
    unsigned i,j;
    for (i = 0; i < m_rowSize; i++)
    {
        for (j = 0; j < m_colSize; j++)
        {
            sum(i,j) = this->m_matrix[i][j] + B(i,j);
        }
    }
    return sum;
}
//Matrix Subtraction
matrixClass matrixClass::operator-(matrixClass B){
    matrixClass diff(m_rowSize, m_colSize, 0.0);
    unsigned i,j;
    for (i = 0; i < m_rowSize; i++)
    {
        for (j = 0; j < m_colSize; j++)
        {
            diff(i,j) = this->m_matrix[i][j] - B(i,j);
        }
    }

    return diff;
}
//Matrix Multiplication
matrixClass matrixClass::operator*(matrixClass B){
    matrixClass multip(m_rowSize,B.getCols(),0.0);
    if(m_colSize == B.getRows())
    {
        unsigned i,j,k;
        double temp = 0.0;
        for (i = 0; i < m_rowSize; i++)
        {
            for (j = 0; j < B.getCols(); j++)
            {
                temp = 0.0;
                for (k = 0; k < m_colSize; k++)
                {
                    temp += m_matrix[i][k] * B(k,j);
                }
                multip(i,j) = temp;
            }
        }
        return multip;
    }
    else
    {
        matrixClass error = {m_rowSize,B.getCols(),-666.0};
        return error;
    }
}
//Matrix Scalar Addition
matrixClass matrixClass::operator+(double scalar){
    matrixClass result(m_rowSize,m_colSize,0.0);
    unsigned i,j;
    for (i = 0; i < m_rowSize; i++)
    {
        for (j = 0; j < m_colSize; j++)
        {
            result(i,j) = this->m_matrix[i][j] + scalar;
        }
    }
    return result;
}
//Matrix Scalar Subtraction
matrixClass matrixClass::operator-(double scalar){
    matrixClass result(m_rowSize,m_colSize,0.0);
    unsigned i,j;
    for (i = 0; i < m_rowSize; i++)
    {
        for (j = 0; j < m_colSize; j++)
        {
            result(i,j) = this->m_matrix[i][j] - scalar;
        }
    }
    return result;
}
//Matrix Scalar Multiplication
matrixClass matrixClass::operator*(double scalar){
    matrixClass result(m_rowSize,m_colSize,0.0);
    unsigned i,j;
    for (i = 0; i < m_rowSize; i++)
    {
        for (j = 0; j < m_colSize; j++)
        {
            result(i,j) = this->m_matrix[i][j] * scalar;
        }
    }
    return result;
}
//Matrix Scalar Division
matrixClass matrixClass::operator/(double scalar){
    matrixClass result(m_rowSize,m_colSize,0.0);
    unsigned i,j;
    for (i = 0; i < m_rowSize; i++)
    {
        for (j = 0; j < m_colSize; j++)
        {
            result(i,j) = this->m_matrix[i][j] / scalar;
        }
    }
    return result;
}
//Matrix transpose
matrixClass matrixClass::transpose()
{
    matrixClass Transpose(m_colSize,m_rowSize,0.0);
    for (unsigned i = 0; i < m_colSize; i++)
    {
        for (unsigned j = 0; j < m_rowSize; j++) {
            Transpose(i,j) = this->m_matrix[j][i];
        }
    }
    return Transpose;
}
//Matrix inverse (only 2by2)
/*matrixClass matrixClass::inv()
{
    matrixClass C = {m_colSize,m_rowSize,0.0};
    if ((m_colSize == 2) && (m_rowSize == 2))
    {
        //Implemented case; supposed that: B is an invertible 2by2 matrix
        matrixClass B = {2,2,1};
        B(0,0) = this->m_matrix[0][0];
        B(0,1) = this->m_matrix[0][1];
        B(1,0) = this->m_matrix[1][0];
        B(1,1) = this->m_matrix[1][1];
        double invDeterminant = 1/(B(0,0)*B(1,1)-B(0,1)*B(1,0));

        C(0,0) = B(1,1);
        C(0,1) = -B(0,1);
        C(1,0) = -B(1,0);
        C(1,1) = B(0,0);
        return C*invDeterminant;
    }
    else
    {
        //Not implemented case
        return C;
    }
}*/
//Matrix Print
void matrixClass::print() const
{
    std::cout << "Matrix: " << std::endl;
    for (unsigned i = 0; i < m_rowSize; i++) {
        for (unsigned j = 0; j < m_colSize; j++) {
            std::cout << "[" << m_matrix[i][j] << "] ";
        }
        std::cout << std::endl;
    }
}

//Returns rows
unsigned matrixClass::getRows() const
{
    return this->m_rowSize;
}
//Returns cols
unsigned matrixClass::getCols() const
{
    return this->m_colSize;
}
// Returns Matrix(x,y)
double& matrixClass::operator()(const unsigned &rowNo, const unsigned & colNo)
{
    return this->m_matrix[rowNo][colNo];
}


